Docker.hlp
------------------------------------------------------------------------------
docker cli - cliente do docker. -> comunica com Docker daemon(pode ser remoto)
docker registry armazena imagens(dvd) -> para executar em vários containers
------------------------------------------------------------------------------

-- Help - Comandos docker - https://docs.docker.com/engine/reference/commandline/docker/
docker help
docker container help
docker container run --help

-- Executa um container do Docker Hub.
docker container run -p 8080:80 nginx
docker container run -p 8081:80 nginx
docker container run -p 80:80 wordpress
docker container run -p 80:80 -d wordpress	=>detach, nao para o terminal
docker container run -p 80:80 --name blogwp wordpress	=>Executa nomeando
docker container run -p 80:80 --rm --name blogwp wordpress	=>Remove ao parar

-- Listar containers
docker container ls  -> os q estao rodando
docker container ls --all	=>todos, incluindo os parados.

-- Ver logs do container -f(folow)
docker container logs -f 86b052ce6cc0

-- stop/start container
docker container stop 86b052ce6cc0
docker container start 86b052ce6cc0

-- Remover um container
docker container rm 86b052ce6cc0
docker container rm 86b052ce6cc0 --force	=>para remover mesmo estando executando.
docker container rm algafood-mysql --force --volumes 	=> remover junto o volume, apaga dados.

-- Remover todos containers
docker container prune

------------------------------------------
Imagens dos containers.

-- Docker registry padrão.
hub.docker.com
	-images e oficiais.

-- Executando imagem do UBUNTU
docker container run --rm -it ubuntu bash
	apt-get
	cat /etc/issue
	ctrl + d 	=>sair do container.

-- Definindo a versão a ser baixada
docker container run --rm -it ubuntu:latest bash	=>padrao latest
docker container run --rm -it ubuntu:14.04 bash		=>versoes->https://hub.docker.com/_/ubuntu
	cat /etc/issue

------------------------------
--  Gerenciando as imagens  --
----

-- Help docker image
docker image help

-- Listar imagens da máquina
docker image ls
docker image ls --all

-- Somente baixar a imagem sem executar.
docker image pull openjdk:8-jre-slim 	=>versoes->https://hub.docker.com/_/openjdk?tab=tags

-- Executar uma imagem já baixada.
docker container run --rm -it openjdk:8-jre-slim bash

-- Excluir uma imagem
docker image rm openjdk:8-jre-slim
docker image rm ubuntu:14.04
docker image rm 68ce2b46ae05
--

---------------------------------
-- Executando container MySql 8.0 	
-- https://hub.docker.com/_/mysql?tab=description

-- Instalando.
docker container run -d -p 3306:3306 -e MYSQL_ALLOW_EMPTY_PASSWORD=yes --name algafood-mysql mysql:8.0
docker container run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=algafood --name algafood-mysql mysql:8.0
ex.variaveis=>	-e MYSQL_USER=root \ 
				-e MYSQL_PASSWORD=algafood ...

-- logs
docker container logs -f algafood-mysql

--
------------------------------------------------
Criando um deploy do jar no com docker.

--Criar jar.
./mvnw clean package
mvn clean install -Dmaven.test.skip

-- Criar arquivo Dockerfile
Dockerfile

--Build/construir a imagem
docker image build -t algafood .	->-t nome:tag(algafood:1.0)

-- Executando imagem
docker container run -rm -p 8080:8080 algafood 		->ou algafood:1.0

-- Criando nova tag para a imagem
docker image tag algafood:1.0 algafood-api:1.0
docker image rm algafood:0.1

------------------------------------------
-- Ciar uma - network 

-- Listar Networks
docker network ls

-- Criar rede para o docker.
docker network create --driver bridge algafood-network

-- VOLUMES no docker  --
docker volume ls

-- Remover container e junto o volume.
docker container rm algafood-mysql --force --volumes

-- Criar novamente o mysql 8.0 com a rede.
docker container run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=algafood --network algafood-network --name algafood-mysql mysql:8.0

-- Criado variável para definir o dbhost
${DB_HOST:172.17.0.1:3306}} 	=>application.properties 

-- Fazer build e criar nova imagem.
mvn clean install -Dmaven.test.skip
docker image build -t algafood-api:0.2
	pode gera dangling images, fica sem tags.

-- Limpar imagens dangling, penduradas
docker image prune --help
docker image prune 	=>limpa somente as penduradas.

-- Executar o novo jar. na rede docker
docker container run --rm -p 8080:8080 -e DB_HOST=algafood-mysql --network algafood-network algafood-api:0.2

----------------------------------------
----  Construir imagem pelo maven.  ----

-- Dockerfile Maven Plugin.
https://github.com/spotify/dockerfile-maven

-- Build profiles
<profiles>
	<profile>
		<id>docker</id>
		<build>
			<plugins>
				<plugin>
					Dockerfile Maven Plugin.
				</plugin>
			</plugins>
		</build>
	</profile>
</profiles>

-- mvn
mvn package -Dmaven.test.skip -Pdocker
--
---------------------------------------------
-- 24.12. Disponibilizando a imagem da aplicação no Docker Hub
-- Mudar a tag
docker image tag algafood-api:latest thiagofa/algafood-api:latest
docker login
UserName: -> Password:
docker push thiagofa/algafood-api:latest
docker image ls
docker container run --rm -p 8080:8080 -e DB_HOST=algafood-mysql --network algafood-network thiagofa/algafood-api
---------------------------------------------
--
https://docs.docker.com/compose/compose-file/compose-file-v3/
https://docs.docker.com/compose/reference/
https://docs.docker.com/compose/compose-file/
--
docker container rm 687e918d06ae --force --volumes
docker container ls
docker volume ls
docker-compose help
docker-compose up -d  -=>detach
docker container ls
docker-compose logs -f
docker-compose down --volumes   -=>para e remove containers, remove também volumes.
--
---------------------------------------------
--
https://github.com/vishnubob/wait-for-it/blob/master/wait-for-it.sh
https://www.loom.com/share/09da78cea7ed4514a9215905eceb3878
--
---------------------------------------------
