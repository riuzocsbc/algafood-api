package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.FormaPagamento;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FormaPagamentoRepositoryTest {

    @Autowired
    FormaPagamentoRepository formaPagamentoRepository;

    @Test
    public void salvarTest(){
        FormaPagamento formaPagamento = new FormaPagamento();
        formaPagamento.setDescricao("Cheque");
        formaPagamento = formaPagamentoRepository.save(formaPagamento);
        Assert.assertNotNull(formaPagamento.getId());
    }

    @Test
    public void buscarPorId(){
        Optional<FormaPagamento> formaPagamento = formaPagamentoRepository.findById(2L);
        Assert.assertTrue(formaPagamento.isPresent());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void remover(){
        FormaPagamento formaPagamento = new FormaPagamento();
        formaPagamento.setId(1L);
        formaPagamentoRepository.delete(formaPagamento);
    }

    @Test
    public void listar(){
        List<FormaPagamento> formaPagamentos = formaPagamentoRepository.findAll();
        Assert.assertTrue(formaPagamentos.size() > 0);
    }

}
