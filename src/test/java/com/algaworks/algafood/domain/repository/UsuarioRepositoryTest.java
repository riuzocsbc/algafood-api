package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.service.CadastroUsuarioService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioRepositoryTest {

    private Usuario usuarioCriado;

    @Autowired
    CadastroUsuarioService usuarioService;

    @Before
    public void setUp() {
        usuarioCriado = criarUsuario();
    }

    private Usuario criarUsuario() {
        Usuario usuario = new Usuario();
        usuario.setNome("Usuario Teste");
        usuario.setEmail(RandomStringUtils.randomAlphabetic(8) + "@teste.com");
        usuario.setSenha("senha");

        return usuarioService.salvar(usuario);
    }

    @Test
    public void _10_buscarPorId() {
        Optional<Usuario> usuario = usuarioService.buscar(usuarioCriado.getId());
        Assert.assertTrue(usuario.isPresent());
    }

    @Test
    public void _20_listar() {
        List<Usuario> usuarios = usuarioService.listar();
        Assert.assertTrue(usuarios.contains(usuarioCriado));
    }

    @Test
    public void _30_remover() {
        usuarioService.excluir(usuarioCriado.getId());
    }

    @Test
    public void _40_usuarioNaoEncontrado_aoBuscarPorId() {
        usuarioService.excluir(usuarioCriado.getId());

        Optional<Usuario> usuario = usuarioService.buscar(usuarioCriado.getId());
        Assert.assertFalse(usuario.isPresent());
    }

}
