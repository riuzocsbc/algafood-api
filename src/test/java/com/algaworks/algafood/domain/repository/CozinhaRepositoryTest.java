package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.Cozinha;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CozinhaRepositoryTest {

    @Autowired
    CozinhaRepository cozinhaRepository;

    @Test
    public void salvarTest(){
        Cozinha cozinha = new Cozinha();
        cozinha.setNome("Chinesa");
        cozinha = cozinhaRepository.save(cozinha);
        Assert.assertNotNull(cozinha.getId());
    }

    @Test
    public void buscarPorId(){
        Optional<Cozinha> cozinha = cozinhaRepository.findById(2L);
        Assert.assertTrue(cozinha.isPresent());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void remover(){
        cozinhaRepository.deleteById(3L);
    }

    @Test
    public void listar(){
        List<Cozinha> cozinhas = cozinhaRepository.findAll();
        Assert.assertTrue(cozinhas.size() > 0);
    }

}
