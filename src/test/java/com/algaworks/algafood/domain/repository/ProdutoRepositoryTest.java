package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.Produto;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.service.CadastroProdutoService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProdutoRepositoryTest {

    private static final Long ID_RESTAURANTE_PRODUTO = 1L;

    private Produto produtoCriado;
    private Restaurante restaurante = new Restaurante();

    @Autowired
    CadastroProdutoService produtoService;

    @Before
    public void setUp() {
        restaurante.setId(ID_RESTAURANTE_PRODUTO);
        produtoCriado = criarProduto();
    }

    private Produto criarProduto() {
        Produto produto = new Produto();
        produto.setNome("Salame Italiano");
        produto.setDescricao("Salame em fatias finas.");
        produto.setPreco(new BigDecimal("31.00"));
        produto.setAtivo(true);
        produto.setRestaurante(restaurante);

        return produtoService.salvar(produto);
    }

    @Test
    public void _10_buscarPorId() {
        Optional<Produto> produto = produtoService.buscar(produtoCriado.getId());
        Assert.assertTrue(produto.isPresent());
    }

    @Test
    public void _20_listar() {
        List<Produto> produtos = produtoService.listarTodosProdutos(ID_RESTAURANTE_PRODUTO);
        Assert.assertTrue(produtos.contains(produtoCriado));
    }

    @Test
    public void _25_ListarAtivos() {
        produtoCriado.setAtivo(false);
        produtoService.salvar(produtoCriado);

        List<Produto> produtos = produtoService.listarAtivosProdutos(ID_RESTAURANTE_PRODUTO);
        Assert.assertFalse(produtos.contains(produtoCriado));
    }

    @Test
    public void _30_remover() {
        produtoService.excluir(ID_RESTAURANTE_PRODUTO, produtoCriado.getId());
    }

    @Test
    public void _40_produtoNaoEncontrado_aoBuscarPorId() {
        produtoService.excluir(ID_RESTAURANTE_PRODUTO, produtoCriado.getId());

        Optional<Produto> produto = produtoService.buscar(produtoCriado.getId());
        Assert.assertFalse(produto.isPresent());
    }

}
