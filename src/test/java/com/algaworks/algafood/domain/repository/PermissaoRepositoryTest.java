package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.Permissao;
import com.algaworks.algafood.domain.service.CadastroPermissaoService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class PermissaoRepositoryTest {
    
    private Permissao permissaoCriada;
    
    @Autowired
    CadastroPermissaoService permissaoService;

    @Before
    public void setUp() {
        permissaoCriada = criarPermissao();
    }
    
    private Permissao criarPermissao() {
        Permissao permissao = new Permissao();
        permissao.setNome("Permissao 1");
        permissao.setDescricao("Teste permissao 1");
        
        return  permissaoService.salvar(permissao);
    }

    @Test
    public void _10_buscarPorId() {
        Optional<Permissao> permissao = permissaoService.buscar(permissaoCriada.getId());
        Assert.assertTrue(permissao.isPresent());
    }

    @Test
    public void _20_listar() {
        List<Permissao> permissaos = permissaoService.listar();
        Assert.assertTrue(permissaos.contains(permissaoCriada));
    }

    @Test
    public void _30_remover() {
        permissaoService.excluir(permissaoCriada.getId());
    }

    @Test
    public void _40_permissaoNaoEncontrado_aoBuscarPorId() {
        permissaoService.excluir(permissaoCriada.getId());

        Optional<Permissao> permissao = permissaoService.buscar(permissaoCriada.getId());
        Assert.assertFalse(permissao.isPresent());
    }

}
