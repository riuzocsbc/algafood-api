package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.*;
import com.algaworks.algafood.domain.service.EmissaoPedidoService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class PedidoRepositoryTest {

    private Pedido pedidoCriado;

    @Autowired
    EmissaoPedidoService pedidoService;

    @Before
    public void setUp() {
        pedidoCriado = criarPedido();
    }

    private Pedido criarPedido() {
        Cidade cidade = new Cidade();
        cidade.setId(1L);
        Endereco endereco = new Endereco();
        endereco.setCidade(cidade);
        endereco.setCep("80220000");
        endereco.setLogradouro("Rua da entrega");
        endereco.setNumero("1111");
        endereco.setBairro("Bairro Nobre");
        FormaPagamento formaPagamento = new FormaPagamento();
        formaPagamento.setId(1L);
        Restaurante restaurante = new Restaurante();
        restaurante.setId(1L);
        Usuario usuario = new Usuario();
        usuario.setId(1L);
        List<ItemPedido> itensPedido = new ArrayList<>();
        ItemPedido item = new ItemPedido();
        item.setId(1L);
        itensPedido.add(item);


        Pedido pedido = new Pedido();
        pedido.setSubtotal(new BigDecimal("122.00"));
        pedido.setTaxaFrete(new BigDecimal("12.00"));
        pedido.setValorTotal(new BigDecimal("10.10"));
        pedido.setRestaurante(restaurante);
        pedido.setCliente(usuario);
        pedido.setFormaPagamento(formaPagamento);
        pedido.setEnderecoEntrega(endereco);
        pedido.setDataCriacao(OffsetDateTime.now()); //OffsetDateTime.parse("2018-12-03T12:30:30+01:00");
        pedido.setItens(itensPedido);

        return pedidoService.salvar(pedido);
    }

    @Test
    public void _10_buscarPorId() {
        Optional<Pedido> pedido = pedidoService.buscar(pedidoCriado.getId());
        Assert.assertTrue(pedido.isPresent());
    }

    @Test
    public void _20_listar() {
        List<Pedido> pedidos = pedidoService.listar();
        Assert.assertTrue(pedidos.contains(pedidoCriado));
    }

    @Test
    public void _30_remover() {
        pedidoService.excluir(pedidoCriado.getId());
    }

    @Test
    public void _40_pedidoNaoEncontrado_aoBuscarPorId() {
        pedidoService.excluir(pedidoCriado.getId());

        Optional<Pedido> pedido = pedidoService.buscar(pedidoCriado.getId());
        Assert.assertFalse(pedido.isPresent());
    }

}
