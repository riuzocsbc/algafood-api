package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.exception.EntidadeEmUsoException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.Endereco;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.service.CadastroRestauranteService;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestauranteRepositoryTest {

    @Autowired
    CadastroRestauranteService restauranteService;

    @Test
    public void salvarTest(){
        Cozinha cozinha = new Cozinha();
        cozinha.setId(1L);
        Cidade cidade = new Cidade();
        cidade.setId(1L);
        Endereco endereco = new Endereco();
        endereco.setCidade(cidade);
        endereco.setCep("80220000");
        endereco.setLogradouro("Rua da entrega");
        endereco.setNumero("1111");
        endereco.setBairro("Bairro Nobre");

        Restaurante restaurante = new Restaurante();
        restaurante.setNome("Chinesa");
        restaurante.setTaxaFrete(new BigDecimal("10.00"));
        restaurante.setCozinha(cozinha);
        restaurante.setEndereco(endereco);
        restaurante = restauranteService.salvar(restaurante);
        Assert.assertNotNull(restaurante.getId());
    }

    @Test
    public void buscarPorId(){
        Optional<Restaurante> restaurante = restauranteService.buscar(2L);
        Assert.assertNotNull(restaurante.get());
    }

    @Test(expected = EntidadeEmUsoException.class)
    public void remover(){
        Restaurante restaurante = new Restaurante();
        restaurante.setId(1L);
        restauranteService.excluir(restaurante.getId());
    }

    @Test
    public void listar(){
        List<Restaurante> restaurantes = restauranteService.listar();
        Assert.assertTrue(restaurantes.size() > 0);
    }

    @Ignore
    @Test
    public void listarMapeamentoManyToONe(){
        List<Restaurante> restaurantes = restauranteService.listar();
        for (Restaurante restaurante : restaurantes) {
            System.out.println(restaurante.getId() + " - " + restaurante.getNome() + " - " + restaurante.getCozinha().getNome());
        }
    }


}
