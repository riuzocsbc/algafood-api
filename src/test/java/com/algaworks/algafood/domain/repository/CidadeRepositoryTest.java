package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.exception.EntidadeEmUsoException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.service.CadastroCidadeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CidadeRepositoryTest {

    @Autowired
    CadastroCidadeService cidadeService;

    @Test
    public void salvarTest(){
        Cidade cidade = new Cidade();
        cidade.setNome("Florianópolis");
        Estado estado = new Estado();
        estado.setId(2L);
        cidade.setEstado(estado);
        cidade = cidadeService.salvar(cidade);
        Assert.assertNotNull(cidade.getId());
    }

    @Test
    public void buscarPorId(){
        Optional<Cidade> cidade = cidadeService.buscar(2L);
        Assert.assertTrue(cidade.isPresent());
    }

    @Test(expected = EntidadeEmUsoException.class)
    public void remover(){
        Cidade cidade = new Cidade();
        cidade.setId(1L);
        cidadeService.excluir(cidade.getId());
    }

    @Test
    public void listar(){
        List<Cidade> cidades = cidadeService.listar();
        Assert.assertTrue(cidades.size() > 0);
    }

}
