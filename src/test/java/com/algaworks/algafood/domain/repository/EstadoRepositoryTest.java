package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.service.CadastroEstadoService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EstadoRepositoryTest {

    @Autowired
    CadastroEstadoService estadoService;

    @Test
    public void salvarTest(){
        Estado estado = new Estado();
        estado.setNome("São Paulo");
        estado = estadoService.salvar(estado);
        Assert.assertNotNull(estado.getId());
    }

    @Test
    public void buscarPorId(){
        Optional<Estado> estado = estadoService.buscar(2L);
        Assert.assertTrue(estado.isPresent());
    }

    @Test
    public void remover(){
        estadoService.excluir(4L);
    }

    @Test
    public void listar(){
        List<Estado> estados = estadoService.listar();
        Assert.assertTrue(estados.size() > 0);
    }

}
