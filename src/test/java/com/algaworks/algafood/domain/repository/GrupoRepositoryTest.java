package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.Grupo;
import com.algaworks.algafood.domain.service.CadastroGrupoService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class GrupoRepositoryTest {

    private Grupo grupoCriado;

    @Autowired
    CadastroGrupoService grupoService;

    @Before
    public void setUp() {
        grupoCriado = criarGrupo();
    }

    private Grupo criarGrupo() {
        Grupo grupo = new Grupo();
        grupo.setNome("Grupo Teste");

        return grupoService.salvar(grupo);
    }

    @Test
    public void _10_buscarPorId() {
        Optional<Grupo> grupo = grupoService.buscar(grupoCriado.getId());
        Assert.assertTrue(grupo.isPresent());
    }

    @Test
    public void _20_listar() {
        List<Grupo> grupos = grupoService.listar();
        Assert.assertTrue(grupos.contains(grupoCriado));
    }

    @Test
    public void _30_remover() {
        grupoService.excluir(grupoCriado.getId());
    }

    @Test
    public void _40_grupoNaoEncontrado_aoBuscarPorId() {
        grupoService.excluir(grupoCriado.getId());

        Optional<Grupo> grupo = grupoService.buscar(grupoCriado.getId());
        Assert.assertFalse(grupo.isPresent());
    }

}
