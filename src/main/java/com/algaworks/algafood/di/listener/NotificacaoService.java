package com.algaworks.algafood.di.listener;

import com.algaworks.algafood.di.service.ClienteAtivadoEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
public class NotificacaoService {

    @EventListener
    @Order(1)
    public void clienteAtivadoListener(ClienteAtivadoEvent event){
        System.out.println("Recebeu event, cliente " + event.getCliente().getNome() + " agora está ativo.");
    }

}
