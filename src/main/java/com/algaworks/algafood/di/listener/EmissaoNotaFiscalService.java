package com.algaworks.algafood.di.listener;

import com.algaworks.algafood.di.service.ClienteAtivadoEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
public class EmissaoNotaFiscalService {

    @EventListener
    @Order(2)
    public void eventClienteAtivadoListener(ClienteAtivadoEvent event){
        System.out.println("Emitir nota fiscal para " + event.getCliente().getNome() + ", recebeu evento, faz algo!!");
    }
}
