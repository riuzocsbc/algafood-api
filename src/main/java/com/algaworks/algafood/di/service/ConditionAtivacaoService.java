package com.algaworks.algafood.di.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConditionAtivacaoService {

    private String ativacaoServiceType = "ativacaoClienteNormalService";

    @Autowired
    private AtivacaoServiceFactory ativacaoServiceFactory;

    public AtivacaoService getAtivacaoService() {
        return ativacaoServiceFactory.getAtivacaoService(ativacaoServiceType);
    }

    public void setAtivacaoServiceType(String ativacaoServiceType) {
        this.ativacaoServiceType = ativacaoServiceType;
    }
}
