package com.algaworks.algafood.di.service;

public interface AtivacaoServiceFactory {
    AtivacaoService getAtivacaoService(String ativacaoServiceType);
}
