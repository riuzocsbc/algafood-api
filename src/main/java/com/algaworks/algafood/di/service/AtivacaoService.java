package com.algaworks.algafood.di.service;

import com.algaworks.algafood.di.modelo.Cliente;

public interface AtivacaoService {
    void ativar(Cliente cliente);
}
