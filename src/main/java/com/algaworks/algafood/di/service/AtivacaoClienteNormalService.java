package com.algaworks.algafood.di.service;

import com.algaworks.algafood.di.modelo.Cliente;
import com.algaworks.algafood.di.notificacao.NivelUrgencia;
import com.algaworks.algafood.di.notificacao.Notificador;
import com.algaworks.algafood.di.notificacao.TipoDoNotificador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class AtivacaoClienteNormalService implements AtivacaoService{

	@TipoDoNotificador(NivelUrgencia.SEM_URGENCIA)
	@Autowired()
	private Notificador notificador;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@PostConstruct
	public void init(){
		System.out.println("PostConstruct - init()");
	}

	@PreDestroy
	public void destroy() {
		System.out.println("PreDestroy - destroy()");
	}

	public void ativar(Cliente cliente) {
		cliente.ativar();

		if (notificador != null) {
			this.notificador.notificar(cliente, "Mensagem a enviar pelo notificador, cadastro ativado.");
		} else {
			System.out.println("Não existe notificador, mas cliente foi ativado.");
		}

		eventPublisher.publishEvent(new ClienteAtivadoEvent(cliente));

	}

}
