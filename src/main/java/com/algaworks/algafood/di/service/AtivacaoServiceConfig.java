package com.algaworks.algafood.di.service;

import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AtivacaoServiceConfig {

        @Bean
        public ServiceLocatorFactoryBean serviceLocatorFactoryBean() {
            ServiceLocatorFactoryBean serviceLocatorFactoryBean = new ServiceLocatorFactoryBean();
            serviceLocatorFactoryBean.setServiceLocatorInterface(AtivacaoServiceFactory.class);
            return serviceLocatorFactoryBean;
        }

}
