package com.algaworks.algafood.infrastructure.repository.service.query;

import com.algaworks.algafood.domain.filter.VendaDiariaFilter;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.StatusPedido;
import com.algaworks.algafood.domain.model.dto.VendaDiaria;
import com.algaworks.algafood.domain.service.query.VendaDiariaQueryService;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class VendaDiariaQueryServiceImpl implements VendaDiariaQueryService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<VendaDiaria> consultarVendasDiarias(VendaDiariaFilter filtro, String timeOffset) {
        var builder = entityManager.getCriteriaBuilder();
        var query = builder.createQuery(VendaDiaria.class);
        var root = query.from(Pedido.class);
        var predicates = new ArrayList<Predicate>();

        var functionConvertTzDataCriacao = builder.function(
                "convert_tz", Date.class, root.get("dataCriacao"),
                builder.literal("+00:00"), builder.literal(timeOffset)
        );

        var functionDateDataCriacao = builder.function("date", Date.class, functionConvertTzDataCriacao);

        var selection = builder.construct(VendaDiaria.class,
                functionDateDataCriacao,
                builder.count(root.get("id")),
                builder.sum(root.get("valorTotal")));

        if (filtro.getRestauranteId() != null) {
            predicates.add(builder.equal(root.get(Pedido.Fields.restaurante), filtro.getRestauranteId()));
        }

        if (filtro.getDataCriacaoInicio() != null) {
            LocalDateTime filtroLocalDateInicio = filtro.getDataCriacaoInicio().toLocalDateTime();
            Date filtroDataInicio = Date.from(filtroLocalDateInicio.toInstant(ZoneOffset.ofHours(-3)));
            predicates.add(builder.greaterThanOrEqualTo(functionConvertTzDataCriacao, filtroDataInicio));
                    //filtro.getDataCriacaoInicio().withHour(0).withMinute(0).withSecond(0).withNano(000000000)));
        }

        if (filtro.getDataCriacaoFim() != null) {
            LocalDateTime filtroLocalDateFim = filtro.getDataCriacaoFim().toLocalDateTime();
            Date filtroDataFim = Date.from(filtroLocalDateFim.toInstant(ZoneOffset.ofHours(-3)));
            predicates.add(builder.lessThanOrEqualTo(functionConvertTzDataCriacao, filtroDataFim));
//                    root.get(Pedido.Fields.dataCriacao),
//                    filtro.getDataCriacaoFim().withHour(23).withMinute(59).withSecond(59).withNano(999999999)));
        }

        predicates.add(root.get("status").in(StatusPedido.CONFIRMADO, StatusPedido.ENTREGUE));

        query.select(selection);
        query.where(predicates.toArray(new Predicate[0]));
        query.groupBy(functionDateDataCriacao);

        return entityManager.createQuery(query).getResultList();
    }

}
