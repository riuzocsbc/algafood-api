package com.algaworks.algafood.jpa;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Cozinha;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

public class AlteracaoCozinhaMain {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new SpringApplicationBuilder(AlgafoodApiApplication.class)
                .web(WebApplicationType.NONE)
                .run(args);

        CadastroCozinha cadastroCozinha = applicationContext.getBean(CadastroCozinha.class);

        Cozinha cozinha = new Cozinha();
        cozinha.setId(1L);
        cozinha.setNome("Brasileira");

        cadastroCozinha.salvar(cozinha);
        //Merge Faz:
        //Hibernate: select cozinha0_.id as id1_0_0_, cozinha0_.nome as nome2_0_0_ from cozinha cozinha0_ where cozinha0_.id=?
        //Hibernate: update cozinha set nome=? where id=?
        //
        //Atenção: Incluído mais um campo e executado o mesmo exemplo, o novo campo foi alterado para branco, não usar desta forma,
        //sempre ter o objeto original para realizar os sets que irão alterar os valores.

        System.out.printf("%d - %s - %s\n", cozinha.getId(), cozinha.getNome(), cozinha.getPrato());
    }
}