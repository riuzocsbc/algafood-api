package com.algaworks.algafood.core.modelmapper;

import com.algaworks.algafood.api.v1.model.input.RestauranteInput;
import com.algaworks.algafood.domain.model.Restaurante;
import org.modelmapper.PropertyMap;

public class RestauranteInputToRestaurantePropertyMap extends PropertyMap<RestauranteInput, Restaurante> {

    @Override
    protected void configure() {
        skip(destination.getId());
    }

}
