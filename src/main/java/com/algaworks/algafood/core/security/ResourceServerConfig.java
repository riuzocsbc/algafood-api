package com.algaworks.algafood.core.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and()
            //.authorizeRequests()
            //    .antMatchers(HttpMethod.POST, "/v1/cozinhas/**").hasAuthority("EDITAR_COZINHAS")
            //    .antMatchers(HttpMethod.PUT, "/v1/cozinhas/**").hasAuthority("EDITAR_COZINHAS")
            //    .antMatchers(HttpMethod.GET, "/v1/cozinhas/**").authenticated()
            //    .anyRequest().denyAll()
            //.and()
            .formLogin().loginPage("/login")
            .and()
            .authorizeRequests()
                .antMatchers("/oauth/**").authenticated()
                .antMatchers("/favicon.ico").permitAll()
             .and()
            .csrf().disable()
            .cors().and()
            .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(jwtAuthenticationConverter());  //.oauth2ResourceServer().opaqueToken();
    }

    private JwtAuthenticationConverter jwtAuthenticationConverter() {
        var jwtAuthenticationConverter = new JwtAuthenticationConverter();

        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(
            jwt -> {
                var authorities = jwt.getClaimAsStringList("authorities");
                if( authorities == null) {
                    authorities = Collections.emptyList();
                }

                log.info("1. jwt.getClaims(): {}", jwt.getClaims());

                var scoppesAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
                Collection<GrantedAuthority> grantedAuthorities = scoppesAuthoritiesConverter.convert(jwt);

                log.info("2. grantedAuthorities: {}", grantedAuthorities);

                Collection<GrantedAuthority> authoritiesUser = authorities.stream()
                                                                .map(SimpleGrantedAuthority::new)
                                                                .collect(Collectors.toList());
                grantedAuthorities.addAll(authoritiesUser);

                log.info("3. grantedAuthorities: {}", grantedAuthorities);

                return grantedAuthorities;
            }
        );

        return jwtAuthenticationConverter;
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

}