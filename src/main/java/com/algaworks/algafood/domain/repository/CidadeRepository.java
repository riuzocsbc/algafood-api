package com.algaworks.algafood.domain.repository;

import com.algaworks.algafood.domain.model.Cidade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//    Create	INSERT
//    Read (Retrieve)	SELECT
//    Update	UPDATE
//    Delete (Destroy)	DELETE

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Long> {

}
