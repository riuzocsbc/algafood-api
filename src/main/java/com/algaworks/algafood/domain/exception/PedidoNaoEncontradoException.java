package com.algaworks.algafood.domain.exception;

public class PedidoNaoEncontradoException extends EntidadeNaoEncontradaException {

    public static final String MSG_PEDIDO_NAO_ENCONTRADO = "Não existe um pedido com código %d.";

    public PedidoNaoEncontradoException(String codigoPedido) {
        super(String.format(MSG_PEDIDO_NAO_ENCONTRADO, codigoPedido));
    }

    public PedidoNaoEncontradoException(Long pedidoId) {
        this(String.format(MSG_PEDIDO_NAO_ENCONTRADO, pedidoId));
    }
}
