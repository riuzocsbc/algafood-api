package com.algaworks.algafood.domain.listener;

import com.algaworks.algafood.domain.event.PedidoConfirmadoEvent;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.service.EnvioEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Slf4j
@Component
public class NotificacaoClientePedidoConfirmadoListener {

    @Autowired
    private EnvioEmailService envioEmailService;

    @TransactionalEventListener()
    public void aoConfirmarPedido(PedidoConfirmadoEvent event) {

//        if (true) throw new IllegalArgumentException();

        log.info("PedidoConfirmado, enviando email para o cliente.");

        Pedido pedido = event.getPedido();

        var mensagem = EnvioEmailService.Mensagem.builder()
        .assunto(pedido.getRestaurante().getNome() + " - Pedido confirmado")
        .corpo("emails/pedido-confirmado.html")
        .variavel("pedido", pedido)
        .destinatario(pedido.getCliente().getEmail())
        .build();

        envioEmailService.enviar(mensagem);
    }

}
