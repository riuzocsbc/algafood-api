package com.algaworks.algafood.domain.service;

import com.algaworks.algafood.domain.exception.EntidadeEmUsoException;
import com.algaworks.algafood.domain.exception.ProdutoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Produto;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CadastroProdutoService {

    public static final String MSG_PRODUTO_EM_USO = "Produto de código %d não pode ser removida pois está em uso.";

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private CadastroRestauranteService restauranteService;

    @Transactional
    public Produto salvar(Produto produto) {
        return produtoRepository.save(produto);
    }

    public Optional<Produto> buscar(Long produtoId) {
        return produtoRepository.findById(produtoId);
    }

    public Produto buscarOuFalhar(Long restauranteId, Long produtoId) {
        return produtoRepository.findById(restauranteId, produtoId)
                .orElseThrow( () -> new ProdutoNaoEncontradoException(restauranteId, produtoId));
    }

    @Transactional
    public void excluir(Long restauranteId, Long produtoId) {
        try {
            produtoRepository.deleteById(produtoId);
            produtoRepository.flush();
        } catch(EmptyResultDataAccessException e) {
            throw new ProdutoNaoEncontradoException(restauranteId, produtoId);

        } catch(DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(String.format(MSG_PRODUTO_EM_USO, produtoId));
        }
    }

    public List<Produto> listarTodosProdutos(Long restauranteId) {
        Restaurante restaurante = restauranteService.buscarOuFalhar(restauranteId);
        return produtoRepository.findTodosByRestaurante(restaurante);
    }

    public List<Produto> listarAtivosProdutos(Long restauranteId) {
        Restaurante restaurante = restauranteService.buscarOuFalhar(restauranteId);
        return produtoRepository.findAtivosByRestaurante(restaurante);
    }

}
