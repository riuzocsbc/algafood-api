package com.algaworks.algafood.api.v1.model.input;

import com.algaworks.algafood.domain.model.Permissao;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GrupoInput {

    @ApiModelProperty(example = "Gerente", required = true)
    @NotBlank
    private String nome;

    private List<Permissao> permissoes = new ArrayList<>();

}
