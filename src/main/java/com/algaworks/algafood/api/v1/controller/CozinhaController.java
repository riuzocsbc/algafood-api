package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.api.v1.assembler.CozinhaInputDisassembler;
import com.algaworks.algafood.api.v1.assembler.CozinhaModelAssembler;
import com.algaworks.algafood.api.v1.model.CozinhaModel;
import com.algaworks.algafood.api.v1.model.input.CozinhaInput;
import com.algaworks.algafood.api.v1.openapi.controller.CozinhaControllerOpenApi;
import com.algaworks.algafood.core.security.CheckSecurity;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.service.CadastroCozinhaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

// GET /cozinhas HTTP/1.1

@Slf4j
@RestController  //usa só este pois já possui os //@Controller //@ResponseBody
@RequestMapping("/v1/cozinhas")  ////(value = {"/cozinhas", "/gastronomias"}, produces = MediaType.APPLICATION_JSON_VALUE)
public class CozinhaController implements CozinhaControllerOpenApi {
    //private static final Logger logger = LoggerFactory.getLogger(CozinhaController.class);

    @Autowired
    private CozinhaRepository cozinhaRepository;

    @Autowired
    private CadastroCozinhaService cadastroCozinhaService;

    @Autowired
    private CozinhaModelAssembler cozinhaModelAssembler;

    @Autowired
    private CozinhaInputDisassembler cozinhaInputDisassembler;

    @Autowired
    private PagedResourcesAssembler<Cozinha> pagedResourcesAssembler;

    // obter a origem public PagedModel<CozinhaModel> listar(HttpServletRequest request) { System.out.println(request.getRequestURI());
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)  //{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    //para falhar o RestAssured->@ResponseStatus(HttpStatus.CREATED)
    //@PreAuthorize("isAuthenticated()")
    //@PodeConsultarCozinhas
    @CheckSecurity.Cozinhas.PodeConsultar
    public PagedModel<CozinhaModel> listar(@PageableDefault(size = 3) Pageable pageable) {
        log.info("authorities: {}", SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        log.info("Listando Cozinhas com {} Registros, pageNumber {}", pageable.getPageSize(), pageable.getPageNumber());

        Page<Cozinha> cozinhasPage = cozinhaRepository.findAll(pageable);

        PagedModel<CozinhaModel> cozinhasPagedModel = pagedResourcesAssembler.toModel(cozinhasPage, cozinhaModelAssembler);

        return cozinhasPagedModel;
    }

    //@ResponseStatus(HttpStatus.OK) já retorna como padrão.
    @GetMapping("/{cozinhaId}")
    //@PreAuthorize("isAuthenticated()")
    //@PodeConsultarCozinhas
    @CheckSecurity.Cozinhas.PodeConsultar
    public CozinhaModel buscar(@PathVariable Long cozinhaId){  //@PathVariable("cozinhaId") Long id) {
        Cozinha cozinha = cadastroCozinhaService.buscarOuFalhar(cozinhaId);
        return cozinhaModelAssembler.toModel(cozinha);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    //@PreAuthorize("hasAuthority('EDITAR_COZINHAS')")
    //@PodeEditarCozinhas
    @CheckSecurity.Cozinhas.PodeEditar
    public CozinhaModel adicionar(@RequestBody @Valid CozinhaInput cozinhaInput) {
        Cozinha cozinha = cozinhaInputDisassembler.toDomainObject(cozinhaInput);
        cozinha = cadastroCozinhaService.salvar(cozinha);

        return cozinhaModelAssembler.toModel(cozinha);
    }

    @PutMapping("/{cozinhaId}")
    @ResponseStatus(HttpStatus.OK)
    //@PreAuthorize("hasAuthority('EDITAR_COZINHAS')")
    //@PodeEditarCozinhas
    @CheckSecurity.Cozinhas.PodeEditar
    public CozinhaModel atualizar(@PathVariable Long cozinhaId, @RequestBody @Valid CozinhaInput cozinhaInput) {
        Cozinha cozinhaAtual = cadastroCozinhaService.buscarOuFalhar(cozinhaId);
        cozinhaInputDisassembler.copyToDomainObject(cozinhaInput, cozinhaAtual);
        cozinhaAtual = cadastroCozinhaService.salvar(cozinhaAtual);

        return cozinhaModelAssembler.toModel(cozinhaAtual);
    }

    @DeleteMapping("/{cozinhaId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    //@PreAuthorize("hasAuthority('EDITAR_COZINHAS')")
    //@PodeEditarCozinhas
    @CheckSecurity.Cozinhas.PodeEditar
    public void remover(@PathVariable Long cozinhaId) {
        cadastroCozinhaService.excluir(cozinhaId);
    }

}
