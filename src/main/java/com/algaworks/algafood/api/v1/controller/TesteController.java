package com.algaworks.algafood.api.v1.controller;

import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/teste")
public class TesteController {

    @Autowired
    CozinhaRepository cozinhaRepository;

    @Autowired
    RestauranteRepository restauranteRepository;

    @GetMapping(value ="cozinhas/por-nome") //, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Cozinha> cozinhasPorNome(@RequestParam("nome") String nome) {
        return cozinhaRepository.findCozinhasByNomeContaining(nome);
    }

    @GetMapping(value ="cozinhas/unica-por-nome") //, produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<Cozinha> cozinhaPorNome(@RequestParam("nome") String nome) {
        return cozinhaRepository.findByNome(nome);
    }

    @GetMapping(value = "cozinhas/exists-por-nome")
    public boolean cozinhaExistPorNome(String nome) {
        return cozinhaRepository.existsByNome(nome);
    }

    @GetMapping(value = "cozinhas/primeira")
    public Optional<Cozinha> buscarPrimeira() {
        return cozinhaRepository.buscarPrimeiro();
    }

    @GetMapping(value="restaurantes/por-taxa-frete")
    public List<Restaurante> restaurantessPorTaxaFrete(@RequestParam("taxaFreteInicial")BigDecimal taxaFreteInicial,
                                                       @RequestParam("taxaFreteFinal")BigDecimal taxaFreteFinal) {
        return restauranteRepository.queryByTaxaFreteBetween(taxaFreteInicial, taxaFreteFinal);
    }

    @GetMapping(value="restaurantes/por-nome-e-cozinhaid")
    public List<Restaurante> restaurantesPorNomeECozinhaId(String nomeRestaurante, Long cozinhaId) {
        return restauranteRepository.buscarPorNomeECozinhaId(nomeRestaurante, cozinhaId);
    }

    @GetMapping(value="restaurantes/primeiro-por-nome")
    public Optional<Restaurante> restaurantePrimeiroPorNome(String nomeRestaurante) {
        return restauranteRepository.findFirstRestauranteByNomeContaining(nomeRestaurante);
    }

    @GetMapping(value="restaurantes/top2-por-nome")
    public List<Restaurante> restaurantesTop2PorNome(String nomeRestaurante) {
        return restauranteRepository.findTop2ByNomeContaining(nomeRestaurante);
    }

    @GetMapping(value="restaurantes/count-por-cozinhaId")
    public int restaurantesPorCozinha(Long cozinhaId) {
        return restauranteRepository.countByCozinhaId(cozinhaId);
    }

    @GetMapping(value="restaurantes/por-nome-e-taxas")
    public List<Restaurante> buscarRestaurantesPorNomeETaxas(@RequestParam(value = "nomeRestaurante", required = false) String nome,
            BigDecimal taxaFreteInicial, BigDecimal taxaFreteFinal) {
        return restauranteRepository.buscar(nome, taxaFreteInicial, taxaFreteFinal);
    }

    @GetMapping("/restaurantes/com-frete-gratis")
    public List<Restaurante> restaurantesComFreteGratis(String nome) {
        return restauranteRepository.buscarComFreteGratis(nome);
    }

    @GetMapping("/restaurantes/primeiro")
    public Optional<Restaurante> restaurantePrimeiro() {
        return restauranteRepository.buscarPrimeiro();
    }

}
