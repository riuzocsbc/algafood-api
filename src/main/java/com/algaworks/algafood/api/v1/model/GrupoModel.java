package com.algaworks.algafood.api.v1.model;

import com.algaworks.algafood.domain.model.Permissao;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.util.ArrayList;
import java.util.List;

@Relation(collectionRelation = "grupos")
@Getter
@Setter
public class GrupoModel extends RepresentationModel<GrupoModel> {

    private Long id;
    private String nome;
    private List<Permissao> permissoes = new ArrayList<>();

}
