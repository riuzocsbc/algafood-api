package com.algaworks.algafood.api.v2.controller;

import com.algaworks.algafood.api.ResourceUriHelper;
import com.algaworks.algafood.api.v2.assembler.CidadeInputDisassemblerV2;
import com.algaworks.algafood.api.v2.assembler.CidadeModelAssemblerV2;
import com.algaworks.algafood.api.v2.model.CidadeModelV2;
import com.algaworks.algafood.api.v2.model.input.CidadeIdInputV2;
import com.algaworks.algafood.api.v2.openapi.controller.CidadeControllerV2OpenApi;
import com.algaworks.algafood.domain.exception.EstadoNaoEncontradoException;
import com.algaworks.algafood.domain.exception.RegraNegocioException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.service.CadastroCidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/v2/cidades", produces = MediaType.APPLICATION_JSON_VALUE)
public class CidadeControllerV2 implements CidadeControllerV2OpenApi {

    @Autowired
    private CadastroCidadeService cidadeService;

    @Autowired
    private CidadeModelAssemblerV2 cidadeModelAssemblerV2;

    @Autowired
    private CidadeInputDisassemblerV2 cidadeInputDisassemblerV2;

    @GetMapping()
    public CollectionModel<CidadeModelV2> listar() {
        List<Cidade> todasCidades = cidadeService.listar();
        return  cidadeModelAssemblerV2.toCollectionModel(todasCidades);
    }

    @Override
    @GetMapping("/{cidadeId}")
    public CidadeModelV2 buscar(@PathVariable Long cidadeId) {
        Cidade cidade = cidadeService.buscarOuFalhar(cidadeId);
        return cidadeModelAssemblerV2.toModel(cidade);
    }

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CidadeModelV2 adicionar(@RequestBody @Valid CidadeIdInputV2 cidadeIdInputV2) {
        try {
            Cidade cidade = cidadeInputDisassemblerV2.toDomainObject(cidadeIdInputV2);
            cidade = cidadeService.salvar(cidade);

            CidadeModelV2 cidadeModelV2 = cidadeModelAssemblerV2.toModel(cidade);

            ResourceUriHelper.addUriInResponseHeader(cidadeModelV2.getIdCidade());

            return cidadeModelV2;
        } catch (EstadoNaoEncontradoException e) {
            throw new RegraNegocioException(e.getMessage(), e);
        }
    }

    @Override
    @PutMapping("/{id}")
    public CidadeModelV2 atualizar(@PathVariable Long id,
            @RequestBody @Valid CidadeIdInputV2 cidadeInputV2) {
        try {
            Cidade cidadeAtual = cidadeService.buscarOuFalhar(id);
            cidadeInputDisassemblerV2.copyToDomainObject(cidadeInputV2, cidadeAtual);
            cidadeAtual = cidadeService.salvar(cidadeAtual);

            return cidadeModelAssemblerV2.toModel(cidadeAtual);
        } catch (EstadoNaoEncontradoException e) {
            throw new RegraNegocioException(e.getMessage(), e);
        }
    }

    @Override
    @DeleteMapping("/{cidadeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long cidadeId) {
        cidadeService.excluir(cidadeId);
    }

}
