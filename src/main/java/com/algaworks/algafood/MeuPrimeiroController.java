package com.algaworks.algafood;

import com.algaworks.algafood.di.modelo.Cliente;
import com.algaworks.algafood.di.service.AtivacaoService;
import com.algaworks.algafood.di.service.ConditionAtivacaoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MeuPrimeiroController {

	private ConditionAtivacaoService conditionAtivacaoService;
	
	public MeuPrimeiroController(ConditionAtivacaoService conditionAtivacaoService) {
		this.conditionAtivacaoService = conditionAtivacaoService;
		
		System.out.println("MeuPrimeiroController com ativacaoClienteService, " + conditionAtivacaoService.toString());
	}

	@GetMapping("/hello/{nivelUrgencia}")
	@ResponseBody
	public String hello(@PathVariable("nivelUrgencia") String nivelUrgencia) {
		Cliente joao = new Cliente("Joao", "joao@email.com", "1234567891");

        AtivacaoService ativacaoService = getAtivacaoServiceById(nivelUrgencia);
        ativacaoService.ativar(joao);
		
		return String.format("Olá, notificado com %s!", nivelUrgencia);
	}

	private AtivacaoService getAtivacaoServiceById(String nivelUrgencia) {
		conditionAtivacaoService.setAtivacaoServiceType(nivelUrgencia);
	    AtivacaoService ativacaoService = conditionAtivacaoService.getAtivacaoService();
	    return ativacaoService;
    }

}
