##  Insere dados logo após a criar as tabelas.

INSERT INTO cozinha(id, nome, prato) VALUES(1, 'Tailandesa', 'Prato Tailandês');
INSERT INTO cozinha(id, nome, prato) VALUES(2, 'Indiana', 'Curry');
INSERT INTO cozinha(id, nome, prato) VALUES(5, 'Chinesa', 'Yakissoba');
INSERT INTO cozinha(id, nome, prato) VALUES (3, 'Argentina', null);
INSERT INTO cozinha(id, nome, prato) VALUES (4, 'Brasileira', null);

INSERT INTO estado(id, nome) VALUES(1, 'Paraná');
INSERT INTO estado(id, nome) VALUES(2, 'Santa Catarina');
INSERT INTO estado(id, nome) VALUES(3, 'Rio Grande do Sul');
INSERT INTO estado(id, nome) VALUES(4, 'Rio Grande do Sul');

INSERT INTO cidade(nome, estadoId) VALUES('Curitiba', 1);
INSERT INTO cidade(nome, estadoId) VALUES('Camboriu', 2);
INSERT INTO cidade(nome, estadoId) VALUES('Porto Alegre', 3);

INSERT INTO restaurante(id, nome, taxa_frete, cozinha_id, data_cadastro, data_atualizacao, endereco_cidade_id, endereco_cep, endereco_logradouro, endereco_numero, endereco_bairro) VALUES(1, 'Restaurante 1', 11.50, 1, utc_timestamp, utc_timestamp, 1, '80100-010', 'Rua Alferes Poli','1000', 'Centro');
INSERT INTO restaurante(id, nome, taxa_frete, cozinha_id, data_cadastro, data_atualizacao) VALUES(2, 'Restaurante 2', 22.50, 2, utc_timestamp, utc_timestamp);
INSERT INTO restaurante(id, nome, taxa_frete, cozinha_id, data_cadastro, data_atualizacao) VALUES(3, 'Restaurante 3', 33.50, 2, utc_timestamp, utc_timestamp);
INSERT INTO restaurante (id, nome, taxa_frete, cozinha_id, data_cadastro, data_atualizacao) VALUES (4, 'Java Steakhouse', 12, 3, utc_timestamp, utc_timestamp);
INSERT INTO restaurante (id, nome, taxa_frete, cozinha_id, data_cadastro, data_atualizacao) VALUES (5, 'Lanchonete do Tio Sam', 11, 4, utc_timestamp, utc_timestamp);
INSERT INTO restaurante (id, nome, taxa_frete, cozinha_id, data_cadastro, data_atualizacao) VALUES (6, 'Bar da Maria', 6, 4, utc_timestamp, utc_timestamp);

INSERT INTO forma_pagamento(descricao) VALUES('Dinheiro');
INSERT INTO forma_pagamento(descricao) VALUES('Débito');
INSERT INTO forma_pagamento(descricao) VALUES('Crédito');

INSERT INTO permissao(nome, descricao) VALUES('SALVAR_COZINHA', 'Permite salvar e atualizar cozinha.')
INSERT INTO permissao(nome, descricao) VALUES('CONSULTAR_COZINHA', 'Permite realizar consultas de cozinhas.')
INSERT INTO permissao(nome, descricao) VALUES('REMOVER_COZINHA', 'Permite remover cozinha.')

INSERT INTO restaurante_forma_pagamento(restaurante_id, forma_pagamento_id) VALUES (1, 1), (1, 2), (1, 3), (2, 3), (3, 2), (3, 3), (4, 1), (4, 2), (5, 1), (5, 2), (6, 3);

INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Porco com molho agridoce', 'Deliciosa carne suína ao molho especial', 78.90, 1, 1);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Camarão tailandês', '16 camarões grandes ao molho picante', 110, 1, 1);

INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Salada picante com carne grelhada', 'Salada de folhas com cortes finos de carne bovina grelhada e nosso molho especial de pimenta vermelha', 87.20, 1, 2);

INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Garlic Naan', 'Pão tradicional indiano com cobertura de alho', 21, 1, 3);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Murg Curry', 'Cubos de frango preparados com molho curry e especiarias', 43, 1, 3);

INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Bife Ancho', 'Corte macio e suculento, com dois dedos de espessura, retirado da parte dianteira do contrafilé', 79, 1, 4);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('T-Bone', 'Corte muito saboroso, com um osso em formato de T, sendo de um lado o contrafilé e do outro o filé mignon', 89, 1, 4);

INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Sanduíche X-Tudo', 'Sandubão com muito queijo, hamburger bovino, bacon, ovo, salada e maionese', 19, 1, 5);

INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Espetinho de Cupim', 'Acompanha farinha, mandioca e vinagrete', 8, 1, 6);
