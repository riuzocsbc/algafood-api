create table cozinha (
  id bigint not null auto_increment,
  nome varchar(60) not null,
  prato varchar(60) null,

  primary key (id)
) engine=InnoDB default charset=utf8;